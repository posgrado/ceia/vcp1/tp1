# Trabajo práctico 1 - Visión por computadora
## Para las imágenes que serán proporcionadas de Tito se pide:
### 1. Implementar el algoritmo de pasaje a coordenadas cromáticas para librarnos de las variaciones de contraste.
El resultado se puede ver en [Chrom-Coord](Chrom-Coord.ipynb)

### 2. Implementar el algoritmo White Patch para librarnos de las diferencias de color de iluminación
El resultado se puede ver en [White-Patch](White-Patch.ipynb)
